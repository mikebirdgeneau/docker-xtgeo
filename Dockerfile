FROM archlinux:base-devel
ENV TZ America/Edmonton

# Update
RUN pacman -Suy --noconfirm

# Install required tools
RUN pacman -Suy --noconfirm curl git sudo

# Create build issue
RUN useradd builduser -m && passwd -d builduser && printf 'builduser ALL=(ALL) ALL\n' | tee -a /etc/sudoers

# Install yay
RUN sudo -u builduser bash -c 'cd ~ && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si --noconfirm'


RUN mkdir /app
WORKDIR /app

# Install python & poetry:
RUN pacman -S --noconfirm python python-pip python-setuptools pyenv
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py >> get-poetry.py && cat get-poetry.py | python -

# Install python 3.8.6
RUN echo 'export PATH="$HOME/.pyenv/bin:$HOME/.poetry/bin:$PATH"' >> $HOME/.bashrc && echo 'eval "$(pyenv init -)"' >> $HOME/.bashrc
RUN source $HOME/.bashrc && pyenv install 3.8.6 && pyenv global 3.8.6

# Install project dependencies:
COPY pyproject.toml /app/pyproject.toml
RUN source $HOME/.bashrc && source $HOME/.poetry/env && poetry install
